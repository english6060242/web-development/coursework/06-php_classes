<?php include 'includes/header.php';

// Console: php -S localhost:3000 
// Browser: localhost:3000/12-loops.php

// While
echo "-------------------------------------------- while ------------------------------------------- <br>";
$i = 0;

while($i < 10) {
    echo $i . "<br>";
    $i++;
}

echo "<br> -------------------------------------------------------------------------------------------- <br>";

// Do While
echo "-------------------------------------------- do while ------------------------------------------- <br>";
$i = 0; // If we valued: $i = 100, the do while loop`would still execute the code once. 

do {
    echo $i . "<br>";
    $i++;
} while($i < 10);

echo "<br>";
echo "<br> -------------------------------------------------------------------------------------------- <br>";

// For loop
echo "-------------------------------------------- for ------------------------------------------- <br>";
for($i = 0; $i < 10; $i++)
{
    echo $i . "<br>";
}

// Exercise: An iterator will take values from 0 to 100.
// If a the iterator multiple of 3, print 'Fizz',
// If a the iterator is multiple of 5, print 'Buzz',
// If a the iterator is multiple of 5 and 3, print 'Fizzbuzz'.

for($i = 0; $i < 100; $i++)
{
    if($i % 15 === 0) {             // This could also be done like this: if(($i % 5 === 0) && ($i % 3 === 0))
        echo $i . "- Fizzbuzz <br>";
    }
    else if($i % 3 === 0) {
        echo $i . "- Fizz <br>";
    }
    else if($i % 5 === 0) {
        echo $i . "- Buzz <br>";
    }
    else {
        echo $i . "<br>";   // If they're not multiple of 3 nor 5, just print them.
    }
}
echo "<br> -------------------------------------------------------------------------------------------- <br>";

// For each
echo "-------------------------------------------- foreach ------------------------------------------- <br>";
$clients = array('Peter', 'John', 'Karen');
foreach ($clients as $client) {
    echo $client . "<br>";
}

echo count($clients) . "<br>";  // count() will give us the length of the array
echo sizeof($clients) . "<br>"; // so will this.

// foreach does not need the length of the array in order to work, unlike the regular for:
// This would be the for equivalent of a foreach:
for($i = 0; $i < count($clients); $i++) {
    echo $clients[$i] . "<br>";
}
echo "<br> -------------------------------------------------------------------------------------------- <br>";

// In php, loops can use {} or : tell when they start and end
echo "-------------------------------------------- while ------------------------------------------- <br>";
$i = 0;

while($i < 10):
    echo $i . "<br>";
    $i++;
endwhile;
echo "<br> -------------------------------------------------------------------------------------------- <br>";

// This way of writing will work with while, for, foreach, if, else, else if, but it cannot be used for do while.
// Also, there is no "endelse", instead it's written like this:
echo "-------------------------------------------- for ------------------------------------------- <br>";
for($i = 0; $i < 100; $i++):
    if($i % 15 === 0):            
        echo $i . "- Fizzbuzz <br>";
    elseif($i % 3 === 0):               // In order to use this notation, we must write "elseif", NOT "else if"
        echo $i . "- Fizz <br>";
    elseif($i % 5 === 0):
        echo $i . "- Buzz <br>";
    else:
        echo $i . "<br>";   
    endif;
endfor;

// foreach working with associative arrays:
$client = [
    'name' => 'Kyle',
    'balance' => 200,
    'type' => 'Premium'
];
echo "<br> -------------------------------------------------------------------------------------------- <br>";

echo "-------------------------------------------- foreach ------------------------------------------- <br>";
foreach( $client as $value ):
    echo $value . "<br>"; 
endforeach;

// We can also reference the names of the array's properties or "keys":
foreach( $client as $key => $value ):
    echo $key . " - " . $value . "<br>"; 
endforeach;

// Another array . foreach example:
$products = [
    [
        'name'      => 'Tablet',
        'price'     => 200,
        'available' => true
    ],
    [
        'name'      => 'Television',
        'price'     => 300,
        'available' => true
    ],
    [
        'name'      => 'Curve Monitor',
        'price'     => 400,
        'available' => false
    ]
];
echo "<br> -------------------------------------------------------------------------------------------- <br>";

echo "-------------------------------------------- foreach, example ------------------------------------------- <br>";
foreach($products as $product) { ?> 
    <!-- Note that we use ?> to end the php code and start writing html since we are using the <li> element, which html can render.
    This way we don't make php process the <li> element, instead we switch to html, which is more efficient. -->
    <li>
        <p>Product: <?php echo $product['name']?></p>
        <p>Price: <?php echo "$" . $product['price']?></p>
        <p><?php echo ($product['available']) ? 'Available' : 'Not Available';?></p> <!-- we use: echo (condition) ? resultTrue : resultFalse; instead of: -->
        <!-- <//?php
            if($product['available']){
                echo "<p>Available</p>";
            } else {
                echo "<p>Unavailable</p>";
            }
        ?>  -->
    </li>          
<?php }

echo "<br> -------------------------------------------------------------------------------------------- <br>";

include 'includes/footer.php';?>

