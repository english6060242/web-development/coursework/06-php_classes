<?php 
include 'includes/header.php';  

// Console: php -S localhost:3000 
// Browser: localhost:3000/15-json_functions.php

$products = [
    [
        'name'      => 'Tablet',
        'price'     => 200,
        'available' => true
    ],
    [
        'name'      => 'Television',
        'price'     => 300,
        'available' => true
    ],
    [
        'name'      => 'Curve Monitor',
        'price'     => 400,
        'available' => false
    ]
];

echo "<pre>";
var_dump($products);
echo "</pre">

// Associative arrays in php are equivalent to objects in JavaScript.
// PHP can communicate with JavaScript using by converting associative arrays to json like strings.
$json_products = json_encode($products);

echo "<pre>";
var_dump($json_products);
echo "</pre">

// Notice that the first var_dump prints an array, while the second one prints a string. JavsScriopt's fetch API will not
// be able to read the PHP array but it will be able to read the json format string provided by json_encode.

$json_products = json_encode($products, JSON_UNESCAPED_UNICODE); // When using json_encode, you may choose among several conversion types.
                                                                 // JSON_UNESCAPED_UNICODE will recognize characters like the spanish accent. Example: "árbol"

$json_array = json_decode($json_products); // json_decode will convert a json like string to a PHP array.

echo "<pre>";
var_dump($json_array);
echo "</pre>";

include 'includes/footer.php';
?>
