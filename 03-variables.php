<?php include 'includes/header.php';

$name = "john"; // $ = This is a variable

// Variables cannot contain special characters nor start with numbers

$_name = "Ethan"; // Unlike JavaScript, php variables can start or end with "_"

echo $name; 
var_dump($name);

$name = "john2"; // Variables declared using $ can be reasigned

define('constant', "This is the constant's value"); // This defined value cannot ba reasigned.

// echo $constant; // Wrong
echo constant;     // Right

const constant2 = "Value2";
echo constant2;

include 'includes/footer.php';

?>