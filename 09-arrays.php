<?php include 'includes/header.php';

// -------------- Indexed Arrays --------------
// Declaration method 1
$shopCart = ['Tablet','TV', 'Computer'];

// View Array content
echo "<pre>";           // "Pre" will display the array in a clear format
var_dump($shopCart);    
echo "<pre>";

// Acces an array's element 
echo $shopCart[1];
echo "<br>";

// Add an element
$shopCart[3] = 'new product';
echo "<pre>";           
var_dump($shopCart);    
echo "<pre>";

// Add an element at the end of the array
array_push($shopCart, 'Headset');
echo "<pre>";           
var_dump($shopCart);    
echo "<pre>";

// Add an element at the begining of the array
array_unshift($shopCart, 'Smartwatch');
echo "<pre>";           
var_dump($shopCart);    
echo "<pre>";

// Declaration method 2
$clients = array('Client 1', 'Client 2', 'Client 3');

echo "<pre>";           
var_dump($clients);    
echo "<pre>";

// Elements can be accessed the same way:
echo $clients[0];

// The same functions are available for the arrays declared using the array() function:
array_push($clients, 'Client 4');
echo "<pre>";           
var_dump($clients);    
echo "<pre>";

// -------------- Associative Arrays --------------
// Declaration (similar to objects in other programming languages)
$client = [
    'name' => 'John',
    'balance' => 200,
    'info' => [
        'type' => 'premium',
        'expiration' => '03/12/2024'
    ]
];

echo "<pre>";           
var_dump($client);    
echo "<pre>";

// Access elements:
echo "<pre>";           
var_dump($client['name']);    
echo "<pre>";

echo $client['balance'];
echo "<br>";

// Access array elements within arrays:
echo $client['info']['type'];
echo "<br>";
echo $client['info']['expiration'];
echo "<br>";

// Add new properties to the array:
$client['code'] = 548625; // The propertie 'code' does not exist => it's created and added at the end
echo "<pre>";           
var_dump($client);    
echo "<pre>";

// If the property already exists, then the value is replaced
$client['code'] = 127467;
echo $client['code'];
echo "<br>";

// isset and empty
$client2 = [];
$client3 = array();
$client4 = array('Peter', 'John', 'Karen');

// empty: Checks whether an array is empty (true) or not (false)
var_dump( empty($client2) ); // Output: bool(true)
echo "<br>";
var_dump( empty($client3) ); // Output: bool(true)
echo "<br>";
var_dump( empty($client4) ); // Output: bool(false)
echo "<br>";

// How to check wether an array is empty is usually asked in job interviews

// isset: Checks wether an array was created or a property was defined
var_dump( isset($client5) ); // Output: bool(false), $clients5 does not exist
echo "<br>";
var_dump( isset($client2) ); // Output: bool(true)
echo "<br>";
var_dump( isset($client3) ); // Output: bool(true)
echo "<br>";
var_dump( isset($client4) ); // Output: bool(true)
echo "<br>";

// check wether a property within an array exists:
$client2 = [
    'name' => "Tyler",
    'balance' => 20
];

var_dump( isset($client2['name']) ); // Output: bool(true)
echo "<br>";
var_dump( isset($client2['surname']) ); // Output: bool(false)
echo "<br>";

// It can also be used to check if an element of an indexed array exists:
var_dump( isset($client4[1]) ); // Output: bool(true)
echo "<br>";
var_dump( isset($client4[3]) ); // Output: bool(false)
echo "<br>";

include 'includes/footer.php';?>