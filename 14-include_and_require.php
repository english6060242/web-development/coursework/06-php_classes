<?php 
// Console: php -S localhost:3000 
// Browser: localhost:3000/14-include_and_require.php

// In your PHP code, you may use other PHP files in order to utilize functions, templates, etc.
// This is useful in various scenarios. For example, let's say a website has 5000 sub-sites and
// they all share a header and a footer. Using separate files and including them in all 5000 PHP files
// avoids having to modify 5000 files each time a change needs to be made to the header or footer.

// There are several ways to include files in our code.
include 'includes/header.php';  // 'include' is usually used for templates.

require 'includes/functions.php';  // 'require' is usually used for files that contain functions crucial for our application.

// The difference is that if a non-existing or not found file is "included", the rest of the code will still work (usually, this depends on PHP configuration).

// include 'includes/header2.php'; // header2 does not exist => displays error, but code still works

// require 'includes/header2.php'; // header2 does not exist => displays error and the code execution stops there.

echo "Hello World! <br>";   // This will not be executed if any required files are missing, but it will be even if an included file is missing.

// There is also "require_once", which will check whether the file was already required, and only if it was not already required will it require such file. 
require_once 'includes/functions.php'; // This will do nothing, since functions.php was already required.

include 'includes/footer.php';
?>
