<?php include 'includes/header.php';

// String
$name = "john"; // String can use either " " or ' '

echo $name; 
var_dump($name);

// Boolean
$loggedIn = false;

//echo $loggedIn;
var_dump($loggedIn);

// Integer
$number = 200;

echo $number;
var_dump($number);

// Floats
$float = 200.5;

echo $float;
var_dump($float);

// Arrays
$array = [1,2];

echo $array;
var_dump($array);

include 'includes/footer.php';?>