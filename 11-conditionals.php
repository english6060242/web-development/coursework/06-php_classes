<?php include 'includes/header.php';

// IF

$loggedIN = true;

if($loggedIN){
    echo "User is logged in";
    echo "<br>";
}
else {
    echo "User is NOT logged in";
    echo "<br>";
}

// ||, &&, !, etc are also valid

$client = array(
    'name' => 'John',
    'balance' => 200,
    'info' => [
        'type' => 'Premoum'
    ]
);

echo "<pre>";           
var_dump($client);    
echo "<pre>";

if(empty($client)){
    echo "The client is empty";
    echo "<br>";
}
else {
    echo "The client is empty";
    echo "<br>";
    if($client['balance'] > 0) {            // Nested if
        echo "The client has some money";
        echo "<br>";
    }
    else {
        echo "The client has NO money";
        echo "<br>";
    }
}

// Notice that we checked whether the client was empty before checking using if($client['balance'] > 0)
// If the $client had no property called 'balance' or this property was empty, this would result in an error.

$client['balance'] = 0;
$client['info']['type'] = 'Regular';

echo "<pre>";           
var_dump($client);    
echo "<pre>";

// else if
if($client['balance'] > 0 ) {
    echo "The client has some money";
    echo "<br>";
} else if ($client['info']['type'] === 'Premium') {
    echo "It's a Premium client";
    echo "<br>";
} else {
    echo "The client is not Premium";
    echo "<br>";
}

// Switch (use this to avoid multiple if, else statements)

$technology = 'PHP';

switch ($technology) {
    case 'PHP':
        echo 'The programming language is PHP';
        echo "<br>";
        break;
    
    case 'JavaScript': 
        echo 'The programming language is JavaScript';
        echo "<br>";
        break;

    default:
        echo 'Unknown programming language';
        echo "<br>";
        break;
}

include 'includes/footer.php';?>