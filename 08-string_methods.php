<?php include 'includes/header.php';

$clientName = "John Keller"; // Blank spaces also count

// Count characters (string length)
echo strlen($clientName);
echo "<br>";
var_dump($clientName);
echo "<br>";

// Remove unecessary blank spaces
$clientName2 = "                John Keller                    "; 
$modifiedName = trim($clientName2);
echo trim($clientName2);
echo "<br>";
echo strlen($clientName2);
echo "<br>";
echo strlen($modifiedName);
echo "<br>";

// Turn to UPPERCASE
echo strtoupper($clientName);
echo "<br>";

// Turn to lowercase
$clientName3 = "ETHAN MAGNUS";
echo strtolower($clientName3);
echo "<br>";

// Strings methods in a programming language can be handy for example when comparisons or database queries are made.
// For example: let's say email addresses do not distinguis between lower or uppercase and we want to allow the user
// to input his/her email address with either upper or lower case:
$mail1 = "mail@mail.com";
$mail2 = "Mail@mail.com";

var_dump($mail1 === $mail1);    // This will tell the user that $mail2 does not belong to an existing user.
echo "<br>";
var_dump(strtolower($mail1) === strtolower($mail1));    // This will recognize the correct email address
echo "<br>";

// Replace
$clientNamename4 = "Mark Tracy";
echo str_replace('Mark', 'M', $clientNamename4); // Replace "Mark" by "M" in the $clientNamename4 variable
echo "<br>";

// Check whether a string exists
echo strpos($clientName, 'John'); // Output = character numer in which 'John' was found
echo "<br>";
echo strpos($clientName2, 'John'); 
echo "<br>";

// Concatenate
// Method 1:
$clientType = "Premium";
echo "The client: " . $clientName . " is " . $clientType;
echo "<br>";

$var1 = "Hello ";
$var2 = "World!";

echo $var1 . $var2;
echo "<br>";

// Method 1:
echo "The client: {$clientName} is {$clientType}"; // This only work using " ", not ' ' 

include 'includes/footer.php';?>