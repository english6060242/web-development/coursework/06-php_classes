<?php include 'includes/header.php';

$num1 = 30;
$num2 = 20;

// Sum
echo $num1 + $num2; // Output = 50

echo "<br>"; // End Line

// Subtraction
echo $num1 - $num2; // Output = 10

echo "<br>";

// Multiply
echo $num1 * $num2; // Output = 600

echo "<br>";

// Divide
echo $num1 / $num2; // Output = 1.5

echo "<br>";

// Power 
echo 2 **3;     // Output = 8
echo "<br>";
echo 2 **4;     // Output = 16
echo "<br>";
echo 5 **3;     // Output = 125

include 'includes/footer.php';?>