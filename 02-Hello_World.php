<h1>Hello World</h1> 
<!-- Open this command on your terminal: 
    php -S localhost:3000 
Then in your browser go to localhost:3000/01-Hello_World.php (localhost:3000/file_name.php) -->

<h2><?php echo "Hello World 2"?></h2>

<?php 
include 'includes/header.php';  // You can create several php files for a website in order in include functions, different parts of the website, etc.

    echo "Helo World 3";
    print "Hello World 4";
    echo ("Hello World 5");
    print("Hello world 6");
    print_r("Hello world 7"); // This one must use ()
    var_dump("Hello World 8"); // Result: string(13) "Hello World 8" 

include 'includes/footer.php';

// Usefull php tools: PHP Intelephense - PHP IntelliSense

?>


