<?php include 'includes/header.php';

$num1 = 20;
$num1++;

echo $num1;     // Output = 21
echo "<br>";
echo $num1++;   // Output = 21
echo "<br>";
echo $num1;     // Output = 22
echo "<br>";
echo ++$num1;   // Output = 23
echo "<br>";
$num1 += 5;
echo +$num1;   // Output = 28
echo "<br>";

$num2 = 30;
$num2--;

echo $num2;     // Output = 29
echo "<br>";
echo $num2--;   // Output = 29
echo "<br>";
echo $num2;     // Output = 28
echo "<br>";
echo --$num2;   // Output = 27
echo "<br>";
$num2 -= 5;
echo $num2;     // Output = 22

include 'includes/footer.php';?>