<?php 

// In PHP you have some alternatives to access databases:
// - MySQLi
// - PDO
// This options provide easy access to databases and they're both safe to use as long as the proper functions are employed.
// They are also fast and have good performance when accesing data.
// They're both included in PHP, you may need to uncomment some line in php.ini int order to enable them, but there is no need to
// install anything more than PHP.

// MySQLi ONLY allows us to connect to MySQL databases.
// All MySQLi functions begin with "mysqli_" (don't use the "mysql_" ones, they are obsolete, they have security issues).
// You may use MySQLi in an Object Oriented way or use functions.

// PDO supports up to 12 different types of databases (MySQL, Oracle, PostgreSQL, SQLite, etc).
// It can ONLY be using Object Oriented syntax.
// It supports named parameters (it let's you tell the database you are going to input a string, number, etc).

// This php file that contains the database's credentials is usually a separate file in the "includes" folder 
// let's pretend this file is called database.php. 

// --------------------------------------- database.php ---------------------------------------------

// Connect to a database
$db = mysqli_connect('localhost', 'root', 'password', 'database_name'); // If succesfull, nothing will show in the browser. If not, an error will be displayed
// This line is an example, it will not work since the password and database_name are incorrect.

// Check wheter the connection was successful
if(!$db) {
    echo "database connection error";
    exit; // The project relies on the connection to the database, hence, if this connection fails we cannot go on executing anything.
}

// The reason for this credentials being in a separate file is that it may be necessary to migrate to a more potent database. And so, instead of 
// replacing the credentials on mysqli_connect(...) every time the database is accesed, a different file is used to this end and it's included (or rather required)
// on all files which use the database.

// --------------------------------------- end database.php ---------------------------------------------

// Files and folders:
// includes (folder)
// |-> database.php
// |-> functions.php
// index.php (file outside the 'includes' folder)

// --------------------------------------- functions.php ---------------------------------------------

// Perform queries
//For the explanation's continuity's sake we will pretend that from now on, the code is written in the functions.php file.
// There are four steps when consulting a database (and an optional fifth)
function obtain_services() {
    try{
        // Import database credentials
        require 'database.php';
        
        // Create SQL Query
        $sql = "SELECT * FROm services;";

        // Send SQL Query to the database / Perform Query
        $query = mysqli_query($db, $sql); // Here, $db (from the included database.php file) is the reference to the database 

        // Access Results
        echo "<pre>";
        var_dump( mysqli_fetch_all($query)); // There are several fetch options. fetch all will bring all of the results, 
        echo "</pre>";                       // assoc only the first one in array format, etc.

        // Close the connection (optional).
        $result = mysqli_close($db); // mysqli_close returns 1 if successful.
        echo $result;
        // Opened connections use server resources, so we should close them. However, when PHP reaches the end of this file,
        // all opened connections will be automatically closed. 

        return $query; // This will allow us to acces the results from index.php

    } catch (\Throwable $th) {  // Throwable is a PHP class that helps us display error (exception) messages
        // throw $th
    }
}

//obtain_services();

// --------------------------------------- end functions.php ---------------------------------------------
?>
<?php 
// Show query results in the website
// --------------------------------------- index.php ---------------------------------------------

require 'includes/functions.php'; // You can also use:

// require './includes/functions.php'; // . = parent directory,

// require __DIR__ . '/includes/functions.php';

//echo __DIR__;  // __DIR__ is a PHP global that prints up to the directory of the file that executes echo __DIR__
               // Actual Output: D:\Workspace\Cursos\Full_Stack_Dev\06-PHP
               // Output if we were building an actual project : D:\Workspace\Cursos\Full_Stack_Dev\06-PHP\porject
//echo __FILE__; // __FILE__ is another PHP global that prints the file and it's absolute path.
               // Output: D:\Workspace\Cursos\Full_Stack_Dev\06-PHP\16-database_access.php
               // Output if we were building an actual project : D:\Workspace\Cursos\Full_Stack_Dev\06-PHP\porject\index.php

// Access SQL query from functions.php:
$query = obtain_services();
echo "<pre>";
var_dump($query);
echo "</pre>";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="services" class="service_list">
        <!-- mysqli_fetch_all provides all the query results but instead of the names of the columns it will show [0], [1], etc -->
        <!-- On the other hand, mysqli_fetch_assoc will include the name of the SQL column (thought it only shows the first result or row) -->
        <!-- Since  mysqli_fetch_assoc brings only the frirst row, in order to display the entire query result, we will have to iterate-->
        <?php
            //echo "<pre>";
            // var_dump( mysqli_fetch_assoc($query) ); // This will bring only the first row of the query result, hence we will need to iterate
            //echo "</pre>";
            while($service = mysqli_fetch_assoc($query)) {?>   <!-- This while will go on until there are no more results (rows) -->
            <div class="service">
                <p class="service_name"><?php echo $service['name']; ?></p>    <!-- $service is the variable defined in the whilr and 'name' is how that key is named on the database -->
                <p class="service_price"><?php echo $service['price']; ?></p>  <!-- 'price' is also the name of a key in the database -->
            </div>
            <?php  } ?> 
            <!-- We are going in and out of php so that the browser handles everything html related and we only involve the php motor when it is necessary -->
    </div>
</body>
</html>

<?php 
// --------------------------------------- end index.php ---------------------------------------------
?>

