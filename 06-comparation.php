<?php include 'includes/header.php';

$num1 = 20;
$num2 = 30;
$num3 = 30;
$num4 = "30";

var_dump($num1 > $num2); // Output = false
echo "<br>";

var_dump($num1 < $num2); // Output = true
echo "<br>";

var_dump($num1 >= $num2); // Output = false
echo "<br>";

var_dump($num1 <= $num2); // Output = true
echo "<br>";

var_dump($num2 == $num4); // Output = true
echo "<br>";

var_dump($num2 === $num4); // Output = false => for === it must be the same value and same type, hence "30" != 30
echo "<br>";

var_dump($num1 <=> $num2); // Output = -1 => If num1 is < to num2 => -1; if num1 == num2 => 0, if num1 > num2 => 1
echo "<br>";

include 'includes/footer.php';?>