<?php include 'includes/header.php';

// Search for elements within an array
$cart = ['Tablet', 'Computer', 'Television'];
var_dump( in_array('Tablet', $cart) );  // Output: bool(true)
echo "<br>";

var_dump( in_array('Smartwatch', $cart) );  // Output: bool(false)
echo "<br>";

// Sort elements within an indexed array
$numbers = array(1,3,5,2);
sort($numbers);     // arrange in ascending order
echo "<pre>";           
var_dump($numbers);    
echo "<pre>";

rsort($numbers);     // arrange in descending order
echo "<pre>";           
var_dump($numbers);    
echo "<pre>";

// Sort elements within an associative array
$client = array(
    'balance' => 200,
    'type' => 'Premium',
    'name' => 'Peter'
);

echo "<pre>";           
var_dump($client);    
echo "<pre>";

asort($client); // asort will arrange the properties within the array first by type (numbers first, then strings, ...) 
                // and later by the properties' names alphabetically 
echo "<pre>";           
var_dump($client);    
echo "<pre>";

arsort($client); // arsort will do the same as asort but the other way around (Z to A).

echo "<pre>";           
var_dump($client);    
echo "<pre>";

ksort($client); // ksort on the other hand will arrange the elements by property name alphabetically (regardless of the type)

echo "<pre>";           
var_dump($client);    
echo "<pre>";

krsort($client); // krsort will do the same as ksort but the other way around (Z to A).

echo "<pre>";           
var_dump($client);    
echo "<pre>";

include 'includes/footer.php';?>