<?php 
declare(strict_types=1);        // This must be here, otherwise vs code will mark an error
include 'includes/header.php';

// Console: php -S localhost:3000 
// Browser: localhost:3000/13-functions.php

echo "-------------------- myAdder --------------------<br>";

function myAdder($num1, $num2) {
    echo $num1 + $num2 . "<br>";
}

myAdder(10, 20);

echo "-------------------- myAdder2 --------------------<br>";
function myAdder2($num1 = 0, $num2 = 0) {   // We can use default values in case the function is called with no arguments
    echo $num1 + $num2 . "<br>";
}

myAdder2(10); // myAdder(10); would result in an error
// myAdder2(10, 'Hello'); would also result in an error. We can use typeing to specify the type of the function's arguments
// We can make php tell us about this error sooner using declare(strict_types=1);

echo "-------------------- myAdder3 --------------------<br>";
function myAdder3(int $num1 = 0, int $num2 = 0) {
    echo $num1 + $num2 . "<br>";
}

myAdder3(20,30);

echo "-------------------- Functions with return values --------------------<br>";

// let's make a function that returns a message according to the user's logged in status, where by default, the user is not logged in.
function isLoggedIn(bool $logged = false) : string {        // Here, string is the type of the retured value. If we return anything else => error.
    if($logged) {
        return "The user is logged in<br>";
    }else {
        return "The user is NOT logged in<br>";
    }
}

$user = isLoggedIn(true);
echo $user . "<br>";

echo "-------------------- Functions with return optional values --------------------<br>";
// Functions can also have optional return values:
function isLoggedIn2(bool $logged = false) : ?string {       
    if($logged) {
        return "The user is logged in<br>";
    }else {
        echo "The user is NOT logged in<br>";   // We are not returning a value here and yet there is no error.
    }
}

$user = isLoggedIn2(false);
echo $user . "<br>";

echo "-------------------- Functions with multiple return value's types --------------------<br>";
// Functions can also have return values with different types:
function isLoggedIn3(bool $logged = false) : string|int { // If the return value is neither string nor int, there will still be an error
    if($logged) {
        return "The user is logged in<br>";
    }else {
        return 20; 
    }
}

$user = isLoggedIn3(false);
echo $user . "<br>";

echo "-------------------- Functions with mamed return values --------------------<br>";
// PHP 8 introduced named parameters, which allows to specify the parameters in any order when calling a  function:
function mySubstractor(int $num1 = 0, int $num2 = 0) {   
    echo $num1 - $num2 . "<br>";
}

mySubstractor(30, 20);              // Output: 10
mySubstractor(num2: 30, num1:20);   // Output: -10

include 'includes/footer.php';?>